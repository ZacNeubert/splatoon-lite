/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package JavaMessage;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Zac
 */
public class MessageNumberTest {
    
    public MessageNumberTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of clone method, of class MessageNumber.
     */
    @Test
    public void testClone() {
        System.out.println("clone");
        MessageNumber instance = MessageNumber.Create();
        MessageNumber result = instance.clone();
        assertEquals(instance, result);
    }

    /**
     * Test of Create method, of class MessageNumber.
     */
    @Test
    public void testResetSeqNumber() {
        System.out.println("Create");
        MessageNumber expResult = MessageNumber.Create();
        MessageNumber.ResetSeqNumber();
        MessageNumber result = MessageNumber.Create();
        assertEquals(expResult.Pid, result.Pid);
    }

    /**
     * Test of SetSeqNumber method, of class MessageNumber.
     */
    @Test
    public void testSetSeqNumber() {
        System.out.println("SetSeqNumber");
        MessageNumber mn = MessageNumber.Create();
        int newValue = 0;
        MessageNumber.SetSeqNumber(newValue);
        assertEquals(mn.Pid, MessageNumber.Create().Pid);
    }

    /**
     * Test of equals method, of class MessageNumber.
     */
    @Test
    public void testEquals() {
        System.out.println("equals");
        MessageNumber instance = MessageNumber.Create();
        boolean expResult = true;
        MessageNumber mnclone = instance.clone();
        assert(instance.equals(mnclone));
        MessageNumber othermn = MessageNumber.Create();
        assert(!instance.equals(othermn));
    }

    /**
     * Test of Compare method, of class MessageNumber.
     */
    @Test
    public void testCompare() {
        System.out.println("Compare");
        MessageNumber first = MessageNumber.Create();
        for(int i=0; i<1000; i++) {
            MessageNumber a = MessageNumber.Create();
            MessageNumber b = MessageNumber.Create();
            
            assert(MessageNumber.Compare(b, a) > 0);
            assert(MessageNumber.Compare(b, first) > 0);
            assert(MessageNumber.Compare(a, first) > 0);
        }
    }
}
