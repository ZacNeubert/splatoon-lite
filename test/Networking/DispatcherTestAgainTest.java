/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Networking;

import Extensions.Ext;
import JavaMessage.Message;
import JavaMessage.MessageNumber;
import JavaMessage.ReplyMessages.LoginReply;
import JavaMessage.ReplyMessages.Reply;
import Networking.Conversation.Conversation;
import Networking.Conversation.ConversationFactory;
import Networking.Conversation.LoginConversation;
import dsoak.Player;
import java.lang.reflect.InvocationTargetException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Zac
 */
public class DispatcherTestAgainTest {
    
    public DispatcherTestAgainTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        Player player = Player.getTestPlayer();
        Player.ActivePlayer = player;
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of Dispatch method, of class Dispatcher.
     */
    @Test
    public void checkDispatching() throws NoSuchMethodException, InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, UnknownHostException {
        List<Conversation> convs = new ArrayList<Conversation>();
        for(int i=0; i<50; i++) 
            convs.add(ConversationFactory.create(LoginConversation.key));
        Player.ActivePlayer.Conversations = convs;
        
        Dispatcher.initForTests();
        
        for(Conversation c : convs) {
            LoginReply m = new LoginReply(Player.ActivePlayer);
            String json = Ext.getJson(m);
            c.ConvId = MessageNumber.Create();
            m.ConvId = c.ConvId;
            m.MsgId = MessageNumber.Create();
            Envelope e = new Envelope(m, new IPAddress(InetAddress.getLocalHost(), 10000));
            Dispatcher.Dispatch(e);
            assert(c.IncomingQueue.size() == 1);
        }
    }
    
}
