/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Networking.Conversation;

import Networking.Envelope;
import dsoak.Player;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Zac
 */
public class ConversationFactoryTest {
    
    public ConversationFactoryTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        Player player = Player.getTestPlayer();
        Player.ActivePlayer = player;
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of initialize method, of class ConversationFactory.
     */
    @Test
    public void testInitialize() {
        System.out.println("initialize");
        Player player = Player.ActivePlayer;
        ConversationFactory.initialize(player);
    }

    /**
     * Test of create method, of class ConversationFactory.
     */
    @Test
    public void testCreate() throws Exception {
        System.out.println("create");
        String key = LoginConversation.key;
        Class loginClass = LoginConversation.class;
        Conversation result = ConversationFactory.create(key);
        assertEquals(result.getClass(), loginClass);
        
        key = GameListConversation.key;
        loginClass = GameListConversation.class;
        result = ConversationFactory.create(key);
        assertEquals(result.getClass(), loginClass);
        
        
        //Set up for joingame test
        Player.ActivePlayer.login();
        Thread.sleep(3000);
        Player.ActivePlayer.getGameInfos();
        Thread.sleep(3000);
        
        key = LogoutConversation.key;
        loginClass = LogoutConversation.class;
        result = ConversationFactory.create(key);
        assertEquals(result.getClass(), loginClass);
    }
    
}
