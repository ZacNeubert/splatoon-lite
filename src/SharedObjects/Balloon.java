package SharedObjects;

//[DataContract]
public class Balloon extends SharedResource
{
    //,"Balloon":{"DigitalSignature":null,"Id":89,"IsFilled":false}
    
    //[DataMember]
    public boolean IsFilled;

    public String ToString()
    {
        return String.format("Balloon %d, IsFilled=%d", Id, IsFilled);
    }
}
