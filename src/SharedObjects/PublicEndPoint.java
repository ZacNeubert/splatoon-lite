package SharedObjects;

/// <summary>

import Networking.IPAddress;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.nio.charset.StandardCharsets;

/// PublicEndPoint
/// 
/// This class is an Adapter (or Wrapper) for .Net's IPEndPoint that provides some additional
/// functionality, including
///     - Keeping track of the IP hostname, not just the IPv4 Address
///     - Deferred, but automatic, DNS lookup of IP hostname and selection of an appropriate IPv4 Address
/// </summary>
//[DataContract]
public class PublicEndPoint
{
    //#region Private Data Members
    private String myHost = "0.0.0.0";               // String reprsentation of Hostname or IP Address
    private boolean needToResolveHostname = false;      // If true, then the hostname needs to be resolved into an IP Address

    private IPAddress myEP;                         // Adaptee (Object being adapted or "wrapped"
    //#endregion

    //#region Constructors
    public PublicEndPoint() throws UnknownHostException
    {
        SetDefaults();
    }

    public PublicEndPoint(String hostnameAndPort) throws UnknownHostException
    {
        SetHostAndPort(hostnameAndPort);
    }
    //#endregion

    //#region Public properties and methods
    /// <summary>
    /// Host property
    /// 
    /// The property to access the host, which can be hostname or String representation of the IP Address
    /// </summary>
    //[DataMember]
    private String Host;
    public String getHost()
    {
        return myHost;
    }
    public void setHost(String value) throws UnknownHostException
    {
        // If the IPEndPoint hasn't been set up, do so now.  Note that this might be the case when creating
        // the Object via deserialization.  The properties will be called before the data member initializers
        // or the default constructor's body.
        if (myEP == null)
            SetDefaults();

        if (!"".equals(value.trim()))
        {
            myHost = value.trim();
            needToResolveHostname = true;
        }
        else
        {
            myHost = "0.0.0.0";
            myEP = new IPAddress(InetAddress.getByAddress(myHost.getBytes(StandardCharsets.UTF_8)), 0);
            needToResolveHostname = false;
        }
    }

    //[DataMember]
    private int Port;
    public int getPort() { 
        return myEP.Port; 
    }
    public void setPort(int port) throws UnknownHostException
    {
        // If the IPEndPoint hasn't been set up, do so now.  Note that this might be the case when creating
        // the Object via deserialization.  The properties will be called before the data member initializers
        // or the default constructor's body.
        if (myEP == null)
            SetDefaults();

        myEP.Port = port;
    }


    private String HostAndPort;
    public String getHostAndPort() { return this.toString(); }
    public void setHostAndPort(String value) throws UnknownHostException {
        SetHostAndPort(value);
    }

    /// <summary>
    /// IPEndPoint Property
    /// t
    /// This property is for convenience in work with .Net IPEndPoint Objects.
    /// </summary>
    private IPAddress IPEndPoint;
    public IPAddress getIPEndPoint() 
    {
        if (needToResolveHostname)
        {
            needToResolveHostname = false;
            myEP = LookupAddress(myHost);
        }
        return myEP;
    }
    public void setIPEndPoint(IPAddress value) throws UnknownHostException
    {
        myEP = (value != null) ?  value : new IPAddress(InetAddress.getByName("0.0.0.0"), 0);
        myHost = myEP.toString();
        needToResolveHostname = false;
    }

    public static IPAddress LookupAddress(String host)
    {
        return null;
        /*InetAddress result = null;
        if (!("".equals(host.trim()))
        {
            IPAddress[] addressList = Dns.GetHostAddresses(host);
            for (int i = 0; i < addressList.Length && result == null; i++)
                if (addressList[i].AddressFamily == System.Net.Sockets.AddressFamily.InterNetwork)
                    result = addressList[i];
        }
        return result;*/
    }

    @Override
    public String toString()
    {
        return String.format("%s:%d", myHost, Port);
    }

    public boolean Equals(Object obj)
    {
        boolean result = false;
        IPAddress other = (IPAddress) obj;
        if (other != null)
        {
            result = (myEP == other);
        }
        return result;
    }
    //#endregion

    //#region Private Methods
    private void SetDefaults() throws UnknownHostException
    {
        this.myHost = "0.0.0.0";
        this.needToResolveHostname = false;
        this.myEP = new IPAddress(InetAddress.getByName("0.0.0.0"), 0);
    }

    private void SetHostAndPort(String hostAndPort) throws UnknownHostException
    {
        SetDefaults();

        if (!("".equals(hostAndPort.trim())))
        {
            String[] tmp = hostAndPort.split(":");
            if (tmp.length == 1)
            {
                Host = hostAndPort;
                needToResolveHostname = true;
            }
            else if (tmp.length >= 2)
            {
                Host = tmp[0].trim();

                int port = 0;
                port = Integer.parseInt(tmp[1].trim());
                Port = port;

                needToResolveHostname = true;
            }
        }
    }
    //#endregion

}
