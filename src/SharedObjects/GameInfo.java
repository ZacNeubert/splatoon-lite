package SharedObjects;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class GameInfo
{
    private ArrayList<ProcessInfo> currentProcesses = null;
    private Object myLock;

    public enum StatusCode { 
        NotInitialized(1), Initializing(2), Available(4),  Starting(8), InProgress(16), Ending(32), Complete(64), Cancelled(128);
        
        private int value;    

        private StatusCode(int value) {
          this.value = value;
        }

        public int getValue() {
          return value;
        }
    };

    public int GameId;
    public String Label;
    public StatusCode Status;
    //public ProcessInfo GameManager;
    public int GameManagerId;
    public int MinPlayers;
    public int MaxPlayers;
    public int[] StartingPlayers;
    public GameProcessData[] CurrentProcesses;
    public int[] Winners;

    public GameInfo()
    {
        //SetupDefaults();
    }

    public GameInfo Clone()
    {
        GameInfo clone = null;
        try {
            clone = (GameInfo) this.clone();
        } catch (CloneNotSupportedException ex) {
            Logger.getLogger(GameInfo.class.getName()).log(Level.SEVERE, null, ex);
        }
        return clone;
    }

    public ProcessInfo FindCurrentProcess(int processId)
    {
        ProcessInfo process = null;
        if (currentProcesses != null)
        {
            if (myLock == null)
                myLock = new Object();

            synchronized(myLock)
            {
                Stream<ProcessInfo> stream = currentProcesses.stream();
                Stream<ProcessInfo> cpStream = stream.filter(p -> p.ProcessId == processId);
                if(cpStream.count() > 0) {
                    process = cpStream.collect(Collectors.toList()).get(0);
                }
                //process = currentProcesses.Find(p => p.ProcessId == processId);
            }
        }
        return process;
    }

    public void AddCurrentProcess(ProcessInfo process)
    {
        if (myLock == null)
            myLock = new Object();

        synchronized(myLock)
        {
            if (process != null)
            {
                if (currentProcesses == null)
                    currentProcesses = new ArrayList<ProcessInfo>();
                currentProcesses.add(process);
            }
        }
    }

    public void RemoveCurrentProcess(int processId)
    {
        if (currentProcesses != null)
        {
            if (myLock == null)
                myLock = new Object();

            synchronized(myLock)
            {
                Stream<ProcessInfo> spstream = currentProcesses.stream();
                spstream = spstream.filter(p -> p.ProcessId != processId);
                currentProcesses = new ArrayList<>(spstream.collect(Collectors.toList()));
                //Basically the same as removing
                //currentProcesses.RemoveAll(p => p.ProcessId == processId);
            }
        }
    }
    
    
    public void asdf() {
        List<String> strs = new ArrayList<String>();
        Stream<String> strStream = strs.stream().filter(st -> st.contains(" "));
        strs = strStream.collect(Collectors.toList());
    }

    private void SetupDefaults()
    {
        if (MinPlayers == 0) MinPlayers = 2;
        if (MaxPlayers == 0) MaxPlayers = 20;
        if ((int) Status.getValue() == 0) Status = StatusCode.NotInitialized;
    }
}