package SharedObjects;
/*public class ResourceSet<T> where T extends SharedResource
{
    private const Dictionary<int, T> _resources = new Dictionary<int, T>(); 
    private readonly List<int> _available = new ArrayList<int>();
    private readonly List<int> _reserved = new ArrayList<int>();
    private readonly List<int> _used = new ArrayList<int>(); 
    private readonly Object _myLock = new Object();

    public void Clear()
    {
        synchronized (_myLock)
        {
            _resources.Clear();
            _reserved.Clear();
            _available.Clear();
            _used.Clear();
        }
    }

    public int AvailableCount
    {
        get
        {
            int result;
            synchronized (_myLock)
            {
                result = _available.Count;
            }
            return result;
        }
    }

    public int UsedCount
    {
        get
        {
            int result;
            synchronized (_myLock)
            {
                result = _used.Count;
            }
            return result;
        }
    }

    public void AddOrUpdate(T sharedResource)
    {
        if (sharedResource != null)
        {
            synchronized (_myLock)
            {
                if (_resources.ContainsKey(sharedResource.Id))
                    _resources[sharedResource.Id] = sharedResource;
                else
                {
                    _resources.Add(sharedResource.Id, sharedResource);
                    _available.Add(sharedResource.Id);
                }
            }
        }
    }

    public T Get(int id)
    {
        T result = null;
        synchronized (_myLock)
        {
            if (_resources.ContainsKey(id))
                result = _resources[id];
        }
        return result;
    }

    public T GetAvailable()
    {
        T result = null;
        synchronized (_myLock)
        {
            if (_available.Count > 0)
                result = _resources[_available[0]];
        }
        return result;
    }

    public T ReserveOne()
    {
        T result = null;
        synchronized (_myLock)
        {
            if (_available.Count > 0)
            {
                result = _resources[_available[0]];
                _available.RemoveAt(0);
                _reserved.Add(result.Id);
            }
        }
        return result;

    }

    public T[] ReserveMany(int n)
    {
        T[] result = null;
        synchronized (_myLock)
        {
            if (n>0 && _available.Count >= n)
            {
                result = new T[n];
                for (int i = 0; i < n; i++)
                {
                    result[i] = _resources[_available[0]];
                    _available.RemoveAt(0);
                    _reserved.Add(result[i].Id);
                }
            }
        }
        return result;

    }

    public void Unreserve(int[] ids)
    {
        if (ids != null && ids.Length > 0)
        {
            foreach (int id in ids)
                Unreserve(id);
        }
    }

    public void Unreserve(int id)
    {
        synchronized (_myLock)
        {
            if (_reserved.Contains(id))
            {
                _reserved.Remove(id);
                _available.Add(id);
            }
        }
    }

    public void MarkAsUsed(int[] ids)
    {
        if (ids != null && ids.Length > 0)
        {
            foreach (int id in ids)
                MarkAsUsed(id);
        }
    }

    public void MarkAsUsed(int id)
    {
        synchronized (_myLock)
        {
            if (_reserved.Contains(id))
                _reserved.Remove(id);
            else if (_available.Contains(id))
                _available.Remove(id);

            if (!_used.Contains(id))
                _used.Add(id);
        }
    }

    public boolean AreAllAvailable(Penny[] pennies)
    {
        boolean result;
        synchronized (_myLock)
        {
            result = true;
            if (pennies != null && pennies.Length > 0)
            {
                if (!pennies.All(p => _available.Contains(p.Id)))
                    result = false;
            }
        }
        return result;
    }

    public boolean AreAnyUsed(Penny[] pennies)
    {
        boolean result = false;
        synchronized (_myLock)
        {
            if (pennies != null && pennies.Any(p => _used.Contains(p.Id)))
                result = true;
        }
        return result;
    }
}*/