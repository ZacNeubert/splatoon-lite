package SharedObjects;
//[DataContract]
public class Penny extends SharedResource
{
    public String toString()
    {
        return String.format("Penny %d", Id);
    }
}