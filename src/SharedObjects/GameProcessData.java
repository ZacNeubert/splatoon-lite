package SharedObjects;

import java.util.Date;

public class GameProcessData
{
    private Date _lastChanged = new Date(System.currentTimeMillis());
    private int _lifePoints;
    private int _hitPoints;
    private boolean _hasUmbrellaRaised;

    private Object _myLock;

    public GameProcessData() {}

    public GameProcessData(ProcessInfo processInfo)
    {
        ProcessId = processInfo.ProcessId;
        Type = processInfo.Type;
    }

    //[DataMember]
    public int ProcessId;

    //[DataMember]
    public int Type;

    //[DataMember]
    public int LifePoints;
    public int getLifePoints() {
            int result;
            if (_myLock==null) _myLock = new Object();               
            synchronized (_myLock)
            {
                result = _lifePoints;
            }
            return result;
    }
    public void setLifePoints(int value)
    {
        if (_myLock == null) _myLock = new Object();
        synchronized (_myLock)
        {
            _lifePoints = value;
            _lastChanged = new Date(System.currentTimeMillis());
        }
    }

    //[DataMember]
    public int HitPoints;
    public int getHitPoints()
    {
        int result;
        if (_myLock == null) _myLock = new Object();
        synchronized (_myLock)
        {
            result = _hitPoints;
        }
        return result;
    }
    public void setHitPoints(int value)
    {
        if (_myLock == null) _myLock = new Object();
        synchronized (_myLock)
        {
            _hitPoints = value;
            _lastChanged = new Date(System.currentTimeMillis());
        }
    }

    //[DataMember]
    public boolean HasUmbrellaRaised;
    public boolean getHasUmbrellaRaised() 
    {
        boolean result;

        if (_myLock == null) _myLock = new Object();
        synchronized (_myLock)
        {
            result = _hasUmbrellaRaised;
        }
        return result;
    }
    public void setHasUmbrellaRaised(boolean value) 
    {
        if (_myLock == null) _myLock = new Object();
        synchronized (_myLock)
        {
            _hasUmbrellaRaised = value;
            _lastChanged = new Date(System.currentTimeMillis());
        }
    }

    public Date getLastChanged() {
        Date result = null;
        if (_myLock == null) _myLock = new Object();
        synchronized (_myLock) {
            result = _lastChanged;
        }
        return result;
    }
    public void ChangeLifePoints(int delta) {
        if (_myLock == null) _myLock = new Object();
        synchronized (_myLock)
        {
            _lifePoints = Math.max(0, _lifePoints + delta);
            _lastChanged = new Date(System.currentTimeMillis());
        }            
    }

    public void ChangeHitPoints(int delta)
    {
        if (_myLock == null) _myLock = new Object();
        synchronized (_myLock)
        {
            _hitPoints = Math.max(0, _hitPoints + delta);
            _lastChanged = new Date(System.currentTimeMillis());
        }
    }

    @Override
    public String toString()
    {
        return String.format(
            "Id=%d, Type=%d, LifePoints=%d, HitPoints=%d, HasUmbrellaRaised=%b",
            ProcessId, Type, LifePoints, HitPoints, HasUmbrellaRaised);
    }
}
