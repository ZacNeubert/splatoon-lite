package SharedObjects;

//[DataContract]
public abstract class SharedResource
{
    //[DataMember]
    public int Id;
    
    public int SignedBy;

    //[DataMember]
    public int[] DigitalSignature;

    public boolean ValidateSignature()
    {
        // TODO: Add RSA parameter(s) and implement
        return true;
    }

    @Override
    public String toString()
    {
        return Id+"";
    }

    //#endregion
}