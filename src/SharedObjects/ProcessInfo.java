package SharedObjects;
//[DataContract]

import Networking.JsonIPEndPoint;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ProcessInfo
{
    public enum ProcessType { Unknown, Registry, GameManager, Player, BalloonStore, WaterServer, UmbrellaManager };
    public enum StatusCode { Unknown, NotInitialized, Initializing, Registered, JoiningGame, JoinedGame, Working, PlayingGame, HostingGame, LeavingGame, Won, Lost, Terminating, Tied};

    public int status;
    private static final String[] statusNames = new String[] { "Unknown", "Not Initialized", "Initializing", "Registered", "Joining Game", "Joined Game", "Working", "Playing Game", "Hosting Game", "Leaving Game", "Won Game", "Lost Game", "Terminating", "Tied in a Draw" };
    private Object myLock = new Object();

    //[DataMember]
    public int ProcessId;
    //[DataMember]
    public int Type;
    //[DataMember]
    public JsonIPEndPoint EndPoint;
    //[DataMember]
    public String Label;
    //[DataMember]
    private int Status;
    public int getStatusCode() {
        int result;
        if (myLock != null)
            synchronized (myLock) { result = status; }
        else
            result = status;
        return result;
    }
    public void setStatusCode(int value)
    {
        if (myLock != null)
            synchronized (myLock) { status = value; }
        else
            status = value;
    }
    private String StatusString;
    public String getStatusString() {
        return statusNames[(int) getStatusCode()];
    }

    //[DataMember]
    public int Wins;
    //[DataMember]
    public int Losses;
    //[DataMember]
    public int Draws;
    //[DataMember]
    public short LifePoints;
    //[DataMember]
    public short HitPoints;
    //[DataMember]
    public short NumberOfPennies;
    //[DataMember]
    public short NumberOfUnfilledBalloon;
    //[DataMember]
    public short NumberOfFilledBalloon;
    //[DataMember]
    public short NumberOfUnraisedUmbrellas;
    //[DataMember]
    public boolean HasUmbrellaRaised;
    //[DataMember]
    public PublicKey PublicKey;

    public Date AliveTimestamp;
    public int AliveReties;

    public ProcessInfo Clone()
    {
        try {
            return (ProcessInfo) this.clone();
        } catch (CloneNotSupportedException ex) {
            Logger.getLogger(ProcessInfo.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    private String LabelAndId;
    public String getLabelAndID() {
        String result = Label.isEmpty() ? Label : "";
        result = result + " ({" + ProcessId + "})";
        return result;
    }

    @Override 
    public String toString()
    {
        return String.format("Id=%d, Label=%s, Type=%d, EndPoint=%s, Status=%d, Wins=%d, Losses=%d, Draws=%d",
                ProcessId, Label, Type,
                (EndPoint == null) ? "" : EndPoint.toString(),
                StatusString,
                Wins, Losses, Draws);
    }
}