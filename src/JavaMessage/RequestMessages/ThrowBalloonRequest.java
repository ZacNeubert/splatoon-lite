package JavaMessage.RequestMessages;

//[DataContract]

import SharedObjects.Balloon;
import dsoak.Player;

public class ThrowBalloonRequest extends Request
{
    String __type = "ThrowBalloonRequest:#Messages.RequestMessages";
    //[DataMember]
    public Balloon Balloon = null;

    //[DataMember]
    public int TargetPlayerId;
    
    public static boolean isRouted() {
        return true;
    }
    
    public ThrowBalloonRequest(Player player) {
        TargetPlayerId = player.balloonTargetProcess;
        while(this.Balloon == null) {
            this.Balloon = player.getAmmo();
        }
    }
}