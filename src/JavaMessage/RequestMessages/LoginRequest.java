package JavaMessage.RequestMessages;

import SharedObjects.IdentityInfo;
import SharedObjects.ProcessInfo;
import dsoak.Player;

public class LoginRequest extends Request
{
    public String __type = "LoginRequest:#Messages.RequestMessages";
    public int ProcessType = 3;
    public String ProcessLabel;
    public IdentityInfo Identity;
    
    public LoginRequest(Player player) {
        ProcessLabel = player.ProcessLabel;
        Identity = player.identityInfo;
    }
}
