package JavaMessage.RequestMessages;
    //[DataContra
import SharedObjects.Balloon;
import SharedObjects.Penny;
import dsoak.Player;
import java.util.logging.Level;
import java.util.logging.Logger;

public class FillBalloonRequest extends Request
{
    String __type = "FillBalloonRequest:#Messages.RequestMessages";
    
    //[DataMember]
    public Balloon Balloon;
    //[DataMember]
    public Penny[] Pennies;
    
    public static boolean isRouted() {
        return true;
    }
    
    public FillBalloonRequest(Player player) {
        Pennies = new Penny[]{player.getPenny(), player.getPenny()};
        this.Balloon = player.getBalloon();
        while(this.Balloon == null) {
            try {
                Thread.sleep(10*player.AmmoBoxSize());
            } catch (InterruptedException ex) {
                Logger.getLogger(FillBalloonRequest.class.getName()).log(Level.SEVERE, null, ex);
            }
            this.Balloon = player.getBalloon();
        }
        while(Pennies[0] == null) {
            try {
                Thread.sleep(100);
            } catch (InterruptedException ex) {
                Logger.getLogger(FillBalloonRequest.class.getName()).log(Level.SEVERE, null, ex);
            }
            Pennies[0] = player.getPenny();
        }
        while(Pennies[1] == null) {
            try {
                Thread.sleep(100);
            } catch (InterruptedException ex) {
                Logger.getLogger(FillBalloonRequest.class.getName()).log(Level.SEVERE, null, ex);
            }
            Pennies[1] = player.getPenny();
        }
    }
}