package JavaMessage.RequestMessages;

import dsoak.Player;

public class GameListRequest extends Request
{
    public String __type = "GameListRequest:#Messages.RequestMessages";
    public int StatusFilter = 4;
    
    public GameListRequest(Player player) {
        super();
    }
}