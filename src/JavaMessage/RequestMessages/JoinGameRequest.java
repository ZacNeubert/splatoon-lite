package JavaMessage.RequestMessages;

import SharedObjects.GameInfo;
import SharedObjects.ProcessInfo;
import dsoak.Player;

public class JoinGameRequest extends Request
{
    String __type = "JoinGameRequest:#Messages.RequestMessages";
    public int GameId;
    public ProcessInfo Process;
    
    public static boolean isRouted() {
        return true;
    }
    
    public JoinGameRequest(Player player) {
        GameInfo gi = player.GameInfos.get(0);
        GameId = gi.GameId;
        Process = player.PlayerInfo;
    }
}
