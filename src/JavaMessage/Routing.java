package JavaMessage;
    //[DataContract]
public class Routing extends Message
{
    String __type = "Routing:#Messages";
    //[DataMember]
    public Message InnerMessage;
    //[DataMember]
    public int[] ToProcessIds;

    public int FromProcessId()
    {
        return (MsgId == null) ? 0 : MsgId.Pid;
    }
    
    private void setMessageNumber() {
        this.ConvId = InnerMessage.ConvId;
        this.MsgId = InnerMessage.MsgId;
    }
    
    public Routing(Message m, int[] tpi) {
        InnerMessage = m; 
        ToProcessIds = tpi;
        setMessageNumber();
    }
    
    public Routing(Message m, int tpi) {
        InnerMessage = m; 
        ToProcessIds = new int[]{tpi};
        setMessageNumber();
    }

}