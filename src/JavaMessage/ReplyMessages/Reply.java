package JavaMessage.ReplyMessages;

import JavaMessage.Message;
import dsoak.Player;

public class Reply extends Message
{
    public String __type="Reply:#Messages.ReplyMessages";
    public Boolean Success;
    public String Note;
    
    public Reply(Player player) {
        Success = true;
        Note = "";
    }
}