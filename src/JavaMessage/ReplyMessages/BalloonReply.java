package JavaMessage.ReplyMessages;

//[DataContract]

import SharedObjects.Balloon;
import dsoak.Player;

public class BalloonReply extends Reply
{
    //[DataMember]
    public Balloon Balloon;
    
    public BalloonReply(Player player) {
        super(player);
    }
}
