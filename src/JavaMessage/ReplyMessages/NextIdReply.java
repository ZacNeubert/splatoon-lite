package JavaMessage.ReplyMessages;
//[DataContract]

import dsoak.Player;

public class NextIdReply extends Reply
{
    //[DataMember]
    public int NextId;
    
    public NextIdReply(Player player) {
        super(player);
    }
}
