package JavaMessage.ReplyMessages;

//[DataContract]

import dsoak.Player;

public class RegisterGameReply extends Reply
{
    public RegisterGameReply(Player player) {
        super(player);
    }
}