package JavaMessage.ReplyMessages;

import SharedObjects.GameInfo;
import dsoak.Player;

public class GameListReply extends Reply
{
//    public String __type;
    public GameInfo[] GameInfo;
    
    public GameListReply(Player player) {
        super(player);
    }
}