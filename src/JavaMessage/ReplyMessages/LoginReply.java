package JavaMessage.ReplyMessages;

import Networking.JsonIPEndPoint;
import SharedObjects.ProcessInfo;
import dsoak.Player;

public class LoginReply extends Reply
{
    public ProcessInfo ProcessInfo;
    public JsonIPEndPoint PennyBankEndPoint;
    public JsonIPEndPoint ProxyEndPoint;
    public String PublicKey;
    
    public LoginReply(Player p) {
        super(p);
    }
}
