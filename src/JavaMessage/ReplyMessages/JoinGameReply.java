package JavaMessage.ReplyMessages;

import dsoak.Player;

public class JoinGameReply extends Reply
{
    public int GameId;
    public int InitialLifePoints;
    
    private JoinGameReply(Player player) {
        super(player);
    }
}
