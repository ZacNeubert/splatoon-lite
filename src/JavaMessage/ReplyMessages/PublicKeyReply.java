package JavaMessage.ReplyMessages;
//[DataContract]

import SharedObjects.PublicKey;
import dsoak.Player;

public class PublicKeyReply extends Reply
{
    //[DataMember]
    public int ProcessId;
    //[DataMember]
    public PublicKey Key;
    
        
    public PublicKeyReply(Player player) {
        super(player);
    }
}