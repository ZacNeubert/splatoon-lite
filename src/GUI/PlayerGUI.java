/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GUI;

import Networking.Conversation.Conversation;
import Networking.Conversation.ConversationFactory;
import Networking.Conversation.LogoutConversation;
import Networking.JsonIPEndPoint;
import SharedObjects.GameInfo;
import SharedObjects.GameProcessData;
import SharedObjects.IdentityInfo;
import SharedObjects.ProcessInfo;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import dsoak.DSoak;
import dsoak.Player;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeListener;
import java.lang.reflect.InvocationTargetException;
import java.sql.Date;
import java.sql.Time;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;
import java.util.TimerTask;
import java.util.Timer;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.Action;

/**
 *
 * @author Zac
 */
public class PlayerGUI extends javax.swing.JFrame {

    public static Player player;
    static Timer timer;
    
    List<String> Messages;
    int messageLimit = 100;
    public synchronized void addMessage(String message) {
        if(Messages == null) {
            Messages = new ArrayList<String>();
        }
        if(message == null) {
            return;
        }
        message = Date.valueOf(LocalDate.now()).toString() + " " + Time.valueOf(LocalTime.now()).toString() + ":  " + message;
        
        Messages.add(0,message);
        int newsize = Messages.size() < messageLimit ? Messages.size() : messageLimit;
        Messages = Messages.subList(0, newsize);
        
        StringBuilder sb = new StringBuilder();
        for(String s : Messages) {
            sb.append(s);
            sb.append("\n");
        }
        jTextArea2.setText(sb.toString());
    }
    
    public void setAttributes(Player player) {
        //IdentityInfo ii, JsonIPEndPoint destip, JsonIPEndPoint jep, JsonIPEndPoint gmep
        StringBuilder sb = new StringBuilder();
        IdentityInfo ii = player.identityInfo;
        if(ii != null) {
            sb.append("ANumber:  ");
            sb.append(ii.ANumber);
            sb.append("\nAlias:    ");
            sb.append(ii.Alias);
            sb.append("\nFirst:    ");
            sb.append(ii.FirstName);
            sb.append("\nLast:     ");
            sb.append(ii.LastName);
        }
        
        sb.append("\nId:       ");
        if(player.PlayerInfo != null) {
            sb.append(player.PlayerInfo.ProcessId);
        }
        
        sb.append("\nState: ");
        sb.append(Player.PlayerState.values()[player.getPlayerState().ordinal()]);
        
        JsonIPEndPoint jep = new JsonIPEndPoint(player.LocalIP);
        if(jep != null) {
            sb.append("\nHost:     ");
            sb.append(jep.Host);
            sb.append(":");
            sb.append(jep.Port);
        }
        JsonIPEndPoint destip = new JsonIPEndPoint(player.DestIP);
        if(destip != null) {
            sb.append("\nRegistry: ");
            sb.append(destip.Host);
            sb.append(":");
            sb.append(destip.Port);
        }
        
        sb.append("\n\nLife: ");
        sb.append(player.LifePoints);
        
        sb.append("\n\nPurchase Attempts: ");
        sb.append(player.PurchaseAttempts);
        
        sb.append("\nMinimum Bid: ");
        sb.append(player.getMinimumBid());
        
        sb.append("\nUnfilled Balloons: ");
        sb.append(player.BalloonBoxSize());
        
        sb.append("\nFilled Balloons: ");
        sb.append(player.AmmoBoxSize());
        
        sb.append("\nThrown Balloons: ");
        sb.append(player.thrownCount());
        
        jTextArea1.setText(sb.toString());
        
        if(player.GameInfos != null && player.GameInfos.size() > 0) {
            StringBuilder sb2 =  new StringBuilder();
            
            sb2.append("Pennies: ");
            sb2.append(player.WalletSize());
            sb2.append("\n");
            
            GameInfo involved = player.GameInfos.get(0);
            sb2.append("Game ID: ");
            sb2.append(player.ActiveGameId);
            sb2.append("\nTarget Process: ");
            sb2.append(player.balloonTargetProcess);
            sb2.append("\n");
            sb2.append("\nBalloon Stores:");
            for(GameProcessData gpd : player.balloonStores) {
                sb2.append("\n  PID: ");
                sb2.append(gpd.ProcessId);
            }
            sb2.append("\nWater Sources:");
            for(GameProcessData gpd : player.waterSources) {
                sb2.append("\n  PID: ");
                sb2.append(gpd.ProcessId);
            }
            sb2.append("\nEnemies:");
            for(GameProcessData gpd : player.enemies) {
                sb2.append("\n  PID: ");
                sb2.append(gpd.ProcessId);
                sb2.append("\n  ");
                sb2.append("Life: ");
                sb2.append(gpd.LifePoints);
                sb2.append("\n  ");
            }
            sb2.append("\nUmbrella Stores");
            for(GameProcessData gpd : player.umbrellaSuppliers) {
                sb2.append("\n  PID: ");
                sb2.append(gpd.ProcessId);
            }
            jTextArea3.setText(sb2.toString());
        }
    }
    
    /**
     * Creates new form PlayerGUI
     */
    public PlayerGUI(Player p) {
        initComponents();
        
        player = p;
    }

    public PlayerGUI() {
        initComponents();
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jScrollPane2 = new javax.swing.JScrollPane();
        jTextArea1 = new javax.swing.JTextArea();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTextArea2 = new javax.swing.JTextArea();
        jButton1 = new javax.swing.JButton();
        jScrollPane3 = new javax.swing.JScrollPane();
        jTextArea3 = new javax.swing.JTextArea();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Player Dialog");

        jLabel1.setText("Player");

        jTextArea1.setEditable(false);
        jTextArea1.setColumns(20);
        jTextArea1.setFont(new java.awt.Font("Courier New", 0, 12)); // NOI18N
        jTextArea1.setRows(5);
        jScrollPane2.setViewportView(jTextArea1);

        jTextArea2.setEditable(false);
        jTextArea2.setColumns(20);
        jTextArea2.setFont(new java.awt.Font("Courier New", 0, 12)); // NOI18N
        jTextArea2.setLineWrap(true);
        jTextArea2.setRows(5);
        jScrollPane1.setViewportView(jTextArea2);

        jButton1.setLabel("Logout");

        jTextArea3.setColumns(20);
        jTextArea3.setRows(5);
        jScrollPane3.setViewportView(jTextArea3);

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 777, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jButton1))
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel1Layout.createSequentialGroup()
                        .addComponent(jScrollPane2)
                        .addGap(18, 18, 18)
                        .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(jButton1))
                .addGap(6, 6, 6)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jScrollPane3, javax.swing.GroupLayout.DEFAULT_SIZE, 251, Short.MAX_VALUE)
                    .addComponent(jScrollPane2))
                .addGap(18, 18, 18)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 370, Short.MAX_VALUE)
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(PlayerGUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(PlayerGUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(PlayerGUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(PlayerGUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        final PlayerGUI gui = new PlayerGUI();
        
        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                gui.setVisible(true);
            }
        });
        
        TimerTask updateGUI = new TimerTask() {
            @Override
            public void run() {
                if(player != null) {
                    try {
                        gui.setAttributes(player);
                    }
                    catch(Exception e) {
                        gui.addMessage(e.toString());
                    }
                    if(player.guiMessages != null) {
                        while(!player.guiMessages.isEmpty()) {
                            String alert = player.guiMessages.poll();
                            if(alert != null) {
                                gui.addMessage(alert);
                                System.out.println(alert);
                            }
                        }
                    }
                }
            }  
        };
        
        DSoak dsoak = new DSoak();
        dsoak.GUI = gui;
        
        timer = new Timer("MyTimer");//create a new Timer

        timer.scheduleAtFixedRate(updateGUI, 0, 300);
        
        gui.jButton1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    Conversation logoutConversation = ConversationFactory.create(LogoutConversation.key);
                    Player.ActivePlayer.addConversation(logoutConversation, "logout");
                    logoutConversation.start();
                } catch (NoSuchMethodException ex) {
                    Logger.getLogger(PlayerGUI.class.getName()).log(Level.SEVERE, null, ex);
                } catch (InstantiationException ex) {
                    Logger.getLogger(PlayerGUI.class.getName()).log(Level.SEVERE, null, ex);
                } catch (IllegalAccessException ex) {
                    Logger.getLogger(PlayerGUI.class.getName()).log(Level.SEVERE, null, ex);
                } catch (IllegalArgumentException ex) {
                    Logger.getLogger(PlayerGUI.class.getName()).log(Level.SEVERE, null, ex);
                } catch (InvocationTargetException ex) {
                    Logger.getLogger(PlayerGUI.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        });
        
        dsoak.start();
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButton1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JTextArea jTextArea1;
    private javax.swing.JTextArea jTextArea2;
    private javax.swing.JTextArea jTextArea3;
    // End of variables declaration//GEN-END:variables
}
