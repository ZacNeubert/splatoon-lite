package Extensions;

import java.io.UnsupportedEncodingException;
import java.lang.Exception;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.Arrays;

import Extensions.Ext;

public class BitConverter
{
	public static byte[] getBytes(boolean x)
	{
		return new byte[]{
				(byte) (x ? 1:0)
		};
	}
	
	public static byte[] getBytes(char c)
	{
		///System.out.println("getBytes(char x)" + c);
		return new byte[]  {
				(byte)(c & 0xff),
				(byte)(c >> 8 & 0xff) };
	}
	
	public static byte[] getBytes(double x)
	{
		//System.out.println("getBytes(double x)" + x);
		return getBytes(
				Double.doubleToRawLongBits(x));
	}
	
	public static byte[] getBytes(short x)
	{
		//System.out.println("getBytes(short x)" + x);
		return new byte[] {
				(byte)(x >>> 8),
				(byte)x
		};
	}
	
	/*public static byte[] getBytes(int x)
	{
		return new byte[] {
			(byte)(x >>> 24),
			(byte)(x >>> 16),
			(byte)(x >>> 8),
			(byte)x
		};
	}*/
	
	public static byte[] getTwoBytes(int x) {
		byte[] b = new byte[2];
		ByteBuffer bb = ByteBuffer.wrap(b);
		bb.putChar((char) x);
		byte[] bswap = new byte[] {bb.array()[1], bb.array()[0]};
		return bswap;
	}
	
	public static byte[] getFourBytes(int x) {
		byte[] b = new byte[4];
		ByteBuffer bb = ByteBuffer.wrap(b);
		bb.putInt(x);
		byte[] bswap = new byte[] {bb.array()[3], bb.array()[2], bb.array()[1], bb.array()[0]};
		L.l.info(""+x);
		L.l.info(""+bswap[0]);
		L.l.info(""+bswap[1]);
		return bswap;
	}
	
	public static byte[] getBufferBytes(int x) {
		return ByteBuffer.allocate(x).array();
	}
	
	public static byte[] getBytes(int x)
	{
		//System.out.println("getBytes(int x)" + x);
		return new byte[] {
			(byte)x,
			(byte)(x >>> 8),
			(byte)(x >>> 16),
			(byte)(x >>> 24)
		};
	}
	
	public static byte[] getSixBytes(int x) {
		return new byte[] {
				(byte)(x >>> 40),
				(byte)(x >>> 32),
				(byte)(x >>> 24),
				(byte)(x >>> 16),
				(byte)(x >>> 8),
				(byte)x
		};
	}
	
	public static byte[] getBytes(long x)
	{
		return new byte[] {
				(byte)(x >>> 56),
				(byte)(x >>> 48),
				(byte)(x >>> 40),
				(byte)(x >>> 32),
				(byte)(x >>> 24),
				(byte)(x >>> 16),
				(byte)(x >>> 8),
				(byte)x
		};
	}
	
	public static int toInt32(byte[] b) throws Exception {
		return ToInt32(b);
	}
	
	public static int ToInt32(byte[] b) throws Exception {
		return toInt32(b, 0);
	}
	
	public static int ToInt64(int x) {
		return toInt64(x);
	}
	
	public static int toInt64(int x) {
		return x;
	}
	
	public static byte[] getBytes(float x)
	{
		return getBytes(
				Float.floatToRawIntBits(x));
	}
	
	public static byte[] getBytes(String x)
	{
		return x.getBytes();
	}
	
	public static long doubleToInt64Bits(double x)
	{
		return Double.doubleToRawLongBits(x);
	}
	
	public static double int64BitsToDouble(long x)
	{
		return (double)x;
	}
	
	public boolean toBoolean(byte[] bytes, int index) throws Exception
	{
		if(bytes.length != 1)
			throw new Exception("The length of the byte array must be at least 1 byte long.");
		return bytes[index] != 0;
	}
	
	public char toChar(byte[] bytes, int index) throws Exception
	{
		if(bytes.length != 2)
			throw new Exception("The length of the byte array must be at least 2 bytes long.");
		return (char)(
				(0xff & bytes[index]) << 8   |
				(0xff & bytes[index + 1]) << 0
				);
	}
	
	public static double ToDouble(byte[] bytes, int index) throws Exception
	{
		//if(bytes.length < 8)
			//throw new Exception("The length of the byte array must be at least 8 bytes long.");
		return Double.longBitsToDouble(
				toInt64(bytes, index));
	}
	
	public static double toDouble(byte[] bytes, int index) throws Exception
	{
		if(bytes.length != 8)
			throw new Exception("The length of the byte array must be at least 8 bytes long.");
		return Double.longBitsToDouble(
				toInt64(bytes, index));
	}
	
	public static Boolean toBool(byte[] bytes, int index) throws Exception {
		//if(bytes.length <= 8)
		//	throw new Exception("The length of the byte array must be at least 8 bytes long.");
		return (bytes[index] & 0xFF) != 0;
	}
	
	public static Boolean toBool(byte byteob) throws Exception {
		return (byteob & 0xFF) != 0;
	}
	
	public static Boolean ToBoolean(byte byteob) throws Exception {
		return (byteob & 0xFF) != 0;
	}
	
	public static short toInt16(byte[] bytes, int index) throws Exception
	{
		return (short)(
				(0xff & bytes[index]) << 8   |
				(0xff & bytes[index + 1]) << 0
				);
	}
	
	public static short toInt16Backwards(byte[] bytes, int index) throws Exception
	{
		return (short)(
				(0xff & bytes[index+1]) << 8   |
				(0xff & bytes[index]) << 0
				);
	}
	
	public static int toUInt16(Boolean b) {
		return b==true ? 1 : 0;
	}
	
	public static byte ToByte(int i) {
		return (byte) i;
	}
	
	public static byte toByte(int i) {
		return ToByte(i);
	}
	
	public static byte toByte(Boolean b) {
		return (byte) (b ? 1 : 0);
	}
	
	public static boolean ToBoolean(int i) {
		return i != 0;
	}
	
	public static double ToDouble(int i) {
		return (double) i;
	}
	
	public static String ToString(byte[] b) throws UnsupportedEncodingException {
		return Ext.byteToAsciiString(b);
	}
	
	public static int toInt32bb(byte[] bytes, int index) throws Exception { 
		byte[] array = Arrays.copyOfRange(bytes, index, index+4);
		ByteBuffer b = ByteBuffer.wrap(array);
		b.order(ByteOrder.LITTLE_ENDIAN);
		return b.getInt();
	}
	
	public static int toUInt32(byte[] bytes, int index) throws Exception { 
		byte[] array = Arrays.copyOfRange(bytes, index, index+4);
		ByteBuffer b = ByteBuffer.wrap(array);
		return b.getChar();
	}
	
	public static int toInt32(byte[] bytes, int index) throws Exception
	{
		if(index+3 >= bytes.length)
			throw new Exception("The length of the byte array must be at least 4 bytes long.");
		//Extensions.debug(Extensions.hexStr(Arrays.copyOfRange(bytes, index, index + 3)), 24);
		return (int)(
				(int)(0xff & bytes[index + 3]) << 56  |
				(int)(0xff & bytes[index + 2]) << 48  |
				(int)(0xff & bytes[index + 1]) << 40  |
				(int)(0xff & bytes[index]) << 32
				);
	}
	
	//           TESTING BACKWARDS JAVA STUFF (WHICH WORKED SO YOU MAY WANT TO KEEP THAT IN MIND FOR THE FUTURE)
	public static int toInt32Forwards(byte[] bytes, int index) throws Exception
	{
		if(bytes.length <= 4)
			throw new Exception("The length of the byte array must be at least 4 bytes long.");
		return (int)(
				(int)(0xff & bytes[index]) << 56  |
				(int)(0xff & bytes[index + 1]) << 48  |
				(int)(0xff & bytes[index + 2]) << 40  |
				(int)(0xff & bytes[index + 3]) << 32
				);
	}
	
	
	public static int ToUInt32(byte[] bytes, int index) throws Exception {
		return toInt32(bytes, index);
	}
	
	public static long toInt64(byte[] bytes, int index) throws Exception
	{
		//if(bytes.length != 8)
		//	throw new Exception("The length of the byte array must be at least 8 bytes long.");
		return (long)(
				(long)(0xff & bytes[index]) << 56  |
				(long)(0xff & bytes[index + 1]) << 48  |
				(long)(0xff & bytes[index + 2]) << 40  |
				(long)(0xff & bytes[index + 3]) << 32  |
				(long)(0xff & bytes[index + 4]) << 24  |
				(long)(0xff & bytes[index + 5]) << 16  |
				(long)(0xff & bytes[index + 6]) << 8   |
				(long)(0xff & bytes[index + 7]) << 0
				);
	}
	
    public static int ToUInt16(byte[] a, int index) throws Exception
    {
    	//return Byte.toUnsignedInt(a[index]);
	     int back = 0;
	     back = (int) ((back << 8) + a[index]);
	     return back;
	}
    
    public static int twoBytesToInt(byte[] b, int i) {
    	return ((int)(b[i+1] & 0xFF) << 8) | ((int)b[i] & 0xFF);
    }
	
	public static float toSingle(byte[] bytes, int index) throws Exception
	{
		if(bytes.length != 4)
			throw new Exception("The length of the byte array must be at least 4 bytes long.");
		return Float.intBitsToFloat(
				toInt32(bytes, index));
	}
	
	public static String toString(byte[] bytes) throws Exception
	{
		if(bytes == null)
			throw new Exception("The byte array must have at least 1 byte.");
		return new String(bytes);
	}
	
	public static String toString(byte[] b, int start, int len) throws Exception {
		byte[] subarray = Arrays.copyOfRange(b, start, start+len);
		return BitConverter.toString(subarray);
	}

	public static int ToInt32(byte b) {
		return (int) b;
	}
}