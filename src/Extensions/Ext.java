package Extensions;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author zacneubert
 */

import java.io.UnsupportedEncodingException;
import java.lang.reflect.Method;

import Extensions.BitConverter;
import JavaMessage.ReplyMessages.LoginReply;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Pattern;

public class Ext {
	public static Object CallStaticMethod(Class<?> type, String MethodName, Object[] param) throws Exception {
		try {
			Class<?>[] parameterTypes = new Class<?>[param.length];
			int index=0;
			for(Object o : param) {
				parameterTypes[index] = o.getClass();
			}
			
			/*debug(""+b.getClass(), 2);
			for(Method m : type.getDeclaredMethods()) {
				debug(m.toString(), 2);
				debug(m.getName(), 2);
				debug(""+m.getParameterTypes(), 2);
			}*/
			Method method = searchForMethod(type, MethodName, param.length);
					//type.getDeclaredMethod(MethodName, parameterTypes);
			return method.invoke(null, param);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Unable to call " + MethodName + " on " + type.getName(), e);
		}
	}
	
	private static Method searchForMethod(Class<?> type, String name, int paramCount) {
	    Method[] methods = type.getMethods();
	    for( int i = 0; i < methods.length; i++ ) {
	        // Has to be named the same of course.
	        if( !methods[i].getName().equals( name ) )
	            continue;

	        Class[] types = methods[i].getParameterTypes();

	        // Does it have the same number of arguments that we're looking for.
	        if( types.length != paramCount )
	            continue;
	        
	        return methods[i];

	        // Check for type compatibility
	        //if( InspectionUtils.areTypesCompatible( types, parms ) )
	        //    return methods[i];
	        //}
	    }
	    return null;
	}
	
	public static Object CallStaticMethod(Class<?> type, String MethodName) throws Exception {
		try {
			Class<?>[] params = new Class<?>[0];
			Method method = type.getDeclaredMethod(MethodName, params);
			return method.invoke(null, params);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Unable to call " + MethodName + " on " + type.getName(), e);
		}
	}
	
	public static Object getStaticPropertyValue(Class<?> type, String PropertyName) throws Exception {
		try {
			return type.getField(PropertyName).get(PropertyName);
		}
		catch (Exception e) {
			throw new Exception("Unable to find " + PropertyName + " on " + type.getName(), e);
		}
	}
	
	public static String byteToAsciiString(byte[] b) throws UnsupportedEncodingException {
		String result = new String(b, "ASCII");
		return result;
	    /*StringBuilder sb = new StringBuilder(b.length);
	    for (int i = 0; i < b.length; ++ i) {
	        if (b[i] < 0) throw new IllegalArgumentException();
	        sb.append((char) b[i]);
	    }
	    return sb.toString();*/
	}
	
	public static String byteToAsciiString(byte[] b, int s, int e) throws UnsupportedEncodingException {
	    /*if(e >= b.length) throw new IllegalArgumentException();
		StringBuilder sb = new StringBuilder(b.length);
	    for (int i = s; i < e; ++ i) {
	        if (b[i] < 0) throw new IllegalArgumentException();
	        sb.append((char) b[i]);
	    }*/
	    //System.out.println("s: " + s + " e: " + e);
		
	    String str = new String(b, "ASCII");
	    return str.substring(s, e);
	}
	
	public static byte[] asciiStringToByte(String s) {
		return s.getBytes();
	}
	
	public static boolean stringIsNullOrEmpty(String s) {
		if(s == null) return true;
		if(s == "") return true;
		return false;
	}
	
	public static int ToMajorVersion(String s) {
		return Integer.parseInt(s.split("\\.")[0]);
	}
	
	public static String hexStr(int x) {
		return hexStr(BitConverter.getBytes((int) x));
	}
	
	public static String hexStr(byte[] bb) {
        int i=0;
        if(bb == null) return "Null byte array";
        
        StringBuilder sb = new StringBuilder();
        StringBuilder lb = new StringBuilder();
        for(byte b : bb) {
        	lb.append(String.format("%02d-", i));
        	i = i+1;
        	sb.append(String.format("%02X-", b));
        }
        return "\n" + lb.toString() + "\n" + sb.toString();
    }
	
	public static void hexDebug(byte[] bb) {
        int i=0;
        StringBuilder sb = new StringBuilder();
        for(byte b : bb) {
        	sb.append(String.format("%02X-", b));
        }
        L.l.info(sb.toString());
    }

    public static String getJson(Object o) {
        Gson g = new Gson();
        String json = g.toJson(o);
        return json;
    }
    
    public static Object decodeJson(String json, Class myClass) {
        try {
            Gson g = new Gson();
            Object decoded = g.fromJson(json.trim(), myClass);
            return decoded;
        }
        catch(Exception e) {
                throw e;
        }
    }
    
    public static String prettyJson(String s) {
        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        JsonParser jp = new JsonParser();
        JsonElement je = jp.parse(s);
        String prettyJson = gson.toJson(je);
        return prettyJson;
    }
    
    public static String extractElement(String json, String key) {
        json=json.trim();
        Gson gson = new GsonBuilder().create();
        JsonParser jp = new JsonParser();
        JsonObject jo = jp.parse(json).getAsJsonObject();
        return gson.toJson(jo.get(key));
    }
    
    //static Pattern pattern = Pattern.compile("InnerElement:\{()\}");
    public static String ghettoExtractElement(String json) {
        int i=json.indexOf("InnerMessage");
        while(json.charAt(i) != '{') {
            char c = json.charAt(i);
            i++;
        }
        int start=i;
        int count=1;
        i++;
        while(count != 0) {
            char c = json.charAt(i);
            if(json.charAt(i) == '{') {
                count++;
            }
            if(json.charAt(i) == '}') {
                count--;
            }
            i++;
        }
        String innerElement = json.substring(start, i);
        return innerElement;
    }
    
    public static int[] getUnsignedInts(byte[] bytes) {
        int size = bytes.length;
        int[] ints = new int[size];
        for(int i=0; i<size; i++) {
            ints[i] = bytes[i] & 0xFF;
        }
        return ints;
    }
    
    public static int[] getUnsignedInts(int[] bytes) {
        int size = bytes.length;
        int[] ints = new int[size];
        for(int i=0; i<size; i++) {
            if(bytes[i] < 0) ints[i] = bytes[i] + 256;
            else ints[i]=bytes[i];
            //ints[i] = bytes[i] & 0xFF;
        }
        return ints;
    }
    
    public static void sleep(long millis) {
        try {
            Thread.sleep(millis);
        } catch (InterruptedException ex) {
            Logger.getLogger(Ext.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
