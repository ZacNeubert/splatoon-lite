/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Extensions;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author zacneubert
 */

//Some logging shortcuts 
public class L {
    public static Logger l = Logger.getGlobal();
    
    public static void Warn(String msg) {
        l.log(Level.WARNING, msg);
    }

    public static void debug(String msg) {
        l.log(Level.INFO, msg);
    }
}
