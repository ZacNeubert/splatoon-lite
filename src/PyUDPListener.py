#!/usr/bin/python

import socket

UDP_IP = "127.0.0.1"
UDP_Port = 12000

sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

sock.bind((UDP_IP, UDP_Port))

while True:
	data,addr = sock.recvfrom(1024)
	print str(addr) + " says: \n\t" + str(data)
	sock.sendto("Hullo", addr)
	print "Sent back a 'Hullo'"
