#!/usr/bin/python

from sys import argv

with open(argv[1], 'r') as infile:
	lines = infile.readlines()

print "package asdf;"
for l in lines:
	l = l.replace("object", "Object")
	l = l.replace(" { get; set; }", ";")
	l = l.replace("using ", "import ")
	l = l.replace("lock", "synchronized")
	l = l.replace("new List", "new ArrayList")
	l = l.replace("[Dat", "//[Dat")
	l = l.replace("string", "String")
	l = l.replace("Int32", "int")
	l = l.replace("Int64", "long")
	l = l.replace("String.Empty", "\"\"")
	l = l.replace("bool ", "boolean ")
	l = l.replace("#region", "//#region")
	l = l.replace("#endregion", "//#endregion")
	l = l.replace("ToString", "toString")
	l = l.replace(":","extends")
	if "namespace" in l: continue
	print l,
