/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Networking;

/**
 *
 * @author Zac
 */
public class JsonIPEndPoint {
    public String Host;
    public int Port;
    
    public JsonIPEndPoint(IPAddress ip) {
        if(ip == null) {
            Host = null;
            Port = -1;
        }
        else {        
            Host = ip.Host.getHostAddress();
            Port = ip.Port;
        }
    }
    
    @Override
    public String toString() {
        return Host + ":" + Port;
    }
    
    @Override
    public boolean equals(Object obj) {
        JsonIPEndPoint otherep = (JsonIPEndPoint) obj;
        return otherep.Host.equals(this.Host) && otherep.Port == this.Port;
    }
}
