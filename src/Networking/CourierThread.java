/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Networking;

/**
 *
 * @author Zac
 */
public class CourierThread extends Thread {
    public Envelope envelope;
    
    public CourierThread(Envelope e) {
        envelope = e;
    }
    
    public void run() {
        Dispatcher.Dispatch(envelope);
    }
}
