/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Networking.Conversation;

import Extensions.Ext;
import Extensions.L;
import JavaMessage.Message;
import JavaMessage.MessageNumber;
import JavaMessage.ReplyMessages.BalloonReply;
import JavaMessage.ReplyMessages.GameListReply;
import JavaMessage.ReplyMessages.JoinGameReply;
import JavaMessage.ReplyMessages.Reply;
import JavaMessage.RequestMessages.BuyBalloonRequest;
import JavaMessage.RequestMessages.FillBalloonRequest;
import JavaMessage.RequestMessages.GameListRequest;
import JavaMessage.RequestMessages.JoinGameRequest;
import JavaMessage.RequestMessages.ThrowBalloonRequest;
import JavaMessage.Routing;
import static Networking.Conversation.Conversation.player;
import Networking.Envelope;
import Networking.IPAddress;
import SharedObjects.Penny;
import dsoak.Player;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Zac
 */
public class ThrowBalloonConversation extends Conversation {
    public static String key = "ThrowBalloonConversation";
    
    public void initialize() {
        try {
            retryTimeout = 2000;
            state=0;
            sendEnvelope(MessageSequence.get(state));
        } catch (NoSuchMethodException ex) {
            L.l.log(Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            L.l.log(Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            L.l.log(Level.SEVERE, null, ex);
        } catch (IllegalArgumentException ex) {
            L.l.log(Level.SEVERE, null, ex);
        } catch (InvocationTargetException ex) {
            L.l.log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            L.l.log(Level.SEVERE, null, ex);
        } catch (InterruptedException ex) {
            L.l.log(Level.SEVERE, null, ex);
        }
    }
    
    public ThrowBalloonConversation() {
        history = new ArrayList<Envelope>();
        IncomingQueue = new ConcurrentLinkedQueue<>();
        if(MessageSequence == null) {
            MessageSequence = new ArrayList<Class>();
            MessageSequence.add(ThrowBalloonRequest.class);
            MessageSequence.add(Reply.class);
        }
        try {
            player = Player.ActivePlayer;
            EndPoint = new IPAddress(player.ProxyJsonEndPoint);
            this.routingTo = player.GameManagerId;
        } catch (UnknownHostException ex) {
            Logger.getLogger(ThrowBalloonConversation.class.getName()).log(Level.SEVERE, null, ex);
        }
        catch (Exception e) {
            Logger.getLogger(ThrowBalloonConversation.class.getName()).log(Level.SEVERE, null, e);
        }
    }
    
    /*public Penny[] pennies;
    public void givePennies(Penny[] pennies) {
        this.pennies = pennies;
    }
    public void givePennies(Penny p1, Penny p2) {
        givePennies(new Penny[]{p1,p2});
    }*/
    
    @Override
    void processEnvelope(Envelope envelope) {
        Class MessageClass = MessageSequence.get(state);
        if(MessageClass == Reply.class) {
            String message = envelope.getActualMessage();
            Reply r = (Reply) Ext.decodeJson(message, MessageClass);
            player.incrementThrown();
            if(r.Success) {
                player.guiMessages.add("Threw balloon at nemesis " + this.routingTo);
            }
            else {
                player.guiMessages.add(""+r.Note);
                int i=0;
                i++;
                //player.addPenny(pennies[0]);
                //player.addPenny(pennies[1]);
            }
            state++;
        }
    }
}
