/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Networking.Conversation;

import Extensions.Ext;
import Extensions.L;
import JavaMessage.ReplyMessages.LoginReply;
import JavaMessage.ReplyMessages.Reply;
import JavaMessage.ReplyMessages.StartGame;
import JavaMessage.RequestMessages.AliveRequest;
import JavaMessage.RequestMessages.GameStatusNotification;
import JavaMessage.RequestMessages.LoginRequest;
import JavaMessage.RequestMessages.ReadyToStart;
import static Networking.Conversation.Conversation.player;
import Networking.Envelope;
import dsoak.Player;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Zac
 */
public class GameStatusConversation extends Conversation {
    public static String key = "ReadyToStartConversation";
    public int state = 0;    
    
    public GameStatusConversation() {
        history = new ArrayList<Envelope>();
        IncomingQueue = new ConcurrentLinkedQueue<>();
        if(MessageSequence == null) {
            MessageSequence = new ArrayList<Class>();
            MessageSequence.add(GameStatusNotification.class);
        }
        EndPoint = player.DestIP;
        retries = Integer.MAX_VALUE;
    }
    
    @Override
    void processEnvelope(Envelope envelope) {
        Class MessageClass = MessageSequence.get(state);
        if(MessageClass == GameStatusNotification.class) {
            state++;
            try {
                String actualMessage =envelope.getActualMessage();
                GameStatusNotification ar = (GameStatusNotification) Ext.decodeJson(envelope.getActualMessage(), MessageClass);
                this.ConvId = ar.ConvId.clone();
                player.CurrentGameInfo = ar.Game;
                player.initializeProcesses(ar.Game.CurrentProcesses);
                
                player.balloonTargetProcess = player.getEnemy();
                
                boolean c1 = ar.Game != null;
                if(c1) {
                    boolean hasWinners = ar.Game.Winners != null;
                    if(hasWinners) {
                        if(ar.Game.Winners.length > 0 && ar.Game.GameId == player.ActiveGameId) {
                            player.setPlayerState(Player.PlayerState.LoggedIn);
                        }
                    }
                }
            } catch (IllegalArgumentException ex) {
                L.l.log(Level.SEVERE, null, ex);
            }
            catch (Exception e) {
                L.l.log(Level.SEVERE, null, e);
            }
        }
    }
    
    @Override
    void initialize() {
    //Not necessary here
    }

    @Override
    public void resend() {
    //super unecessary
    }
    
}
