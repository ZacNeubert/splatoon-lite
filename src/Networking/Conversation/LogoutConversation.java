/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Networking.Conversation;

import Extensions.Ext;
import Extensions.L;
import JavaMessage.ReplyMessages.LoginReply;
import JavaMessage.ReplyMessages.Reply;
import JavaMessage.RequestMessages.LogoutRequest;
import static Networking.Conversation.Conversation.player;
import Networking.Envelope;
import dsoak.Player;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Zac
 */
public class LogoutConversation extends Conversation {
        public static String key = "LogoutRequest:#Messages.RequestMessages";
    
    public void initialize() {
        Player.ActivePlayer.setPlayerState(Player.PlayerState.LoggingOut);
        this.state = 0;
        try {
            sendEnvelope(MessageSequence.get(state));
            System.exit(0);
        } catch (NoSuchMethodException ex) {
            L.l.log(Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            L.l.log(Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            L.l.log(Level.SEVERE, null, ex);
        } catch (IllegalArgumentException ex) {
            L.l.log(Level.SEVERE, null, ex);
        } catch (InvocationTargetException ex) {
            L.l.log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            L.l.log(Level.SEVERE, null, ex);
        } catch (InterruptedException ex) {
            L.l.log(Level.SEVERE, null, ex);
        }
    }
    
    public LogoutConversation() {
        history = new ArrayList<Envelope>();
        IncomingQueue = new ConcurrentLinkedQueue<>();
        if(MessageSequence == null) {
            MessageSequence = new ArrayList<Class>();
            MessageSequence.add(LogoutRequest.class);
        }
        EndPoint = player.DestIP;
    }
    
    /*@Override
    public void resend() {}*/
    
    @Override
    void processEnvelope(Envelope envelope) {
        Class MessageClass = MessageSequence.get(state);
        if(MessageClass == Reply.class) {
            Reply reply = (Reply) Ext.decodeJson(envelope.Message, MessageClass);
            if(reply.Success) {
                Player.ActivePlayer.setPlayerState(Player.PlayerState.Exit);
            }
            System.exit(0);
            state++;
        }
    }
}
