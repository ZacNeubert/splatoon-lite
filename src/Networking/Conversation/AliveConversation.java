/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Networking.Conversation;

import Extensions.Ext;
import Extensions.L;
import JavaMessage.ReplyMessages.LoginReply;
import JavaMessage.ReplyMessages.Reply;
import JavaMessage.RequestMessages.AliveRequest;
import JavaMessage.RequestMessages.LoginRequest;
import static Networking.Conversation.Conversation.player;
import Networking.Envelope;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Zac
 */
public class AliveConversation extends Conversation {
    public static String key = "AliveRequest:#Messages.RequestMessages";
    public int state = 0;    
    
    public AliveConversation() {
        history = new ArrayList<Envelope>();
        IncomingQueue = new ConcurrentLinkedQueue<>();
        if(MessageSequence == null) {
            MessageSequence = new ArrayList<Class>();
            MessageSequence.add(AliveRequest.class);
            MessageSequence.add(Reply.class);
        }
        EndPoint = player.DestIP;
        retries = Integer.MAX_VALUE;
    }
    
    @Override
    void processEnvelope(Envelope envelope) {
        Class MessageClass = MessageSequence.get(state);
        if(MessageClass == AliveRequest.class) {
            state++;
            try {
                AliveRequest ar = (AliveRequest) Ext.decodeJson(envelope.getActualMessage(), AliveRequest.class);
                this.ConvId = ar.ConvId.clone();
                sendEnvelope(MessageSequence.get(state));
            } catch (NoSuchMethodException ex) {
                L.l.log(Level.SEVERE, null, ex);
            } catch (InstantiationException ex) {
                L.l.log(Level.SEVERE, null, ex);
            } catch (IllegalAccessException ex) {
                L.l.log(Level.SEVERE, null, ex);
            } catch (IllegalArgumentException ex) {
                L.l.log(Level.SEVERE, null, ex);
            } catch (InvocationTargetException ex) {
                L.l.log(Level.SEVERE, null, ex);
            } catch (IOException ex) {
                L.l.log(Level.SEVERE, null, ex);
            } catch (InterruptedException ex) {
                L.l.log(Level.SEVERE, null, ex);
            }
        }
    }

    @Override
    void initialize() {
    //Not necessary here
    }

    @Override
    public void resend() {
    //super unecessary
    }
    
}
