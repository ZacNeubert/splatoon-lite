/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Networking.Conversation;

import Extensions.Ext;
import Extensions.L;
import JavaMessage.ReplyMessages.LoginReply;
import JavaMessage.ReplyMessages.Reply;
import JavaMessage.ReplyMessages.RoutedReply;
import JavaMessage.ReplyMessages.StartGame;
import JavaMessage.RequestMessages.AliveRequest;
import JavaMessage.RequestMessages.ExitGameRequest;
import JavaMessage.RequestMessages.HitNotification;
import JavaMessage.RequestMessages.LoginRequest;
import JavaMessage.RequestMessages.ReadyToStart;
import static Networking.Conversation.Conversation.player;
import Networking.Envelope;
import SharedObjects.GameInfo;
import dsoak.Player;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.logging.Level;
import java.util.logging.Logger;
import Networking.IPAddress;
import java.net.UnknownHostException;

/**
 *
 * @author Zac
 */
public class ExitGameConversation extends Conversation {
    public static String key = "ExitGameConversation";
    public int state = 0;    
    
    public ExitGameConversation() {
        history = new ArrayList<Envelope>();
        IncomingQueue = new ConcurrentLinkedQueue<>();
        if(MessageSequence == null) {
            MessageSequence = new ArrayList<Class>();
            MessageSequence.add(ExitGameRequest.class);
            MessageSequence.add(RoutedReply.class);
        }
        try {
            EndPoint = new IPAddress(player.ProxyJsonEndPoint);
        } catch (UnknownHostException ex) {
            Logger.getLogger(ExitGameConversation.class.getName()).log(Level.SEVERE, null, ex);
        }
        retries = Integer.MAX_VALUE;
    }
    
    public int GameId;
    @Override
    void processEnvelope(Envelope envelope) {
        Class MessageClass = MessageSequence.get(state);
        if(MessageClass == ExitGameRequest.class) {
            state++;
            try {
                ExitGameRequest r = (ExitGameRequest) Ext.decodeJson(envelope.getActualMessage(), MessageClass);
                this.ConvId = r.ConvId.clone();
                
                player.setPlayerState(Player.PlayerState.LoggedIn);
                
                int pid = player.GameManagerId;
                player.guiMessages.add("Game Over");
                this.routingTo = pid;
                if(this.routingTo <= 0) {
                    throw new Exception("No GameManager?");
                }
                sendEnvelope(MessageSequence.get(state));
            } catch (NoSuchMethodException ex) {
                L.l.log(Level.SEVERE, null, ex);
            } catch (InstantiationException ex) {
                L.l.log(Level.SEVERE, null, ex);
            } catch (IllegalAccessException ex) {
                L.l.log(Level.SEVERE, null, ex);
            } catch (IllegalArgumentException ex) {
                L.l.log(Level.SEVERE, null, ex);
            } catch (InvocationTargetException ex) {
                L.l.log(Level.SEVERE, null, ex);
            } catch (IOException ex) {
                L.l.log(Level.SEVERE, null, ex);
            } catch (InterruptedException ex) {
                L.l.log(Level.SEVERE, null, ex);
            } catch (Exception ex) {
                Logger.getLogger(ExitGameConversation.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    @Override
    void initialize() {
    //Not necessary here
    }

    @Override
    public void resend() {
    //super unecessary
    }
    
}
