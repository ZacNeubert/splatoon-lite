/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Networking.Conversation;

import JavaMessage.Message;
import JavaMessage.MessageNumber;
import JavaMessage.Routing;
import Networking.Envelope;
import Networking.IPAddress;
import Networking.UdpClient;
import dsoak.Player;
import java.io.IOException;
import java.lang.ProcessBuilder.Redirect.Type;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.List;
import java.util.Queue;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Zac
 */
public abstract class Conversation extends Thread {
    public List<Envelope> history;
    public Queue<Envelope> IncomingQueue;
    public IPAddress EndPoint;
    
    public static Player player;
    public String key;
    
    public MessageNumber ConvId;
    
    int state;
    public List<Class> MessageSequence;
    abstract void initialize();
    abstract void processEnvelope(Envelope envelope);
    
    @Override
    public void run() {
        initialize();
        processItems();
    }
    
    public Envelope previousMessage;
    public int routingTo = -1;
    public void sendEnvelope(Class c) throws NoSuchMethodException, InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, IOException, InterruptedException {
        Message message = (Message) c.getConstructor(Player.class).newInstance(player);
        Method m = c.getMethod("isRouted");
        Boolean isRouted = (Boolean) m.invoke(null, null);

        if(state == 0 && this.ConvId == null) {
            this.ConvId = MessageNumber.Create();
            message.MsgId = this.ConvId;
        }
        else {
            message.MsgId = MessageNumber.Create();
        }
        message.ConvId = this.ConvId;
        if(isRouted) {
            message = new Routing(message, routingTo); //probably
        }
        Envelope envelope = new Envelope(message, EndPoint);
        previousMessage = envelope;
        //player.guiMessages.add("Sending " + envelope.Message + " to " + envelope.Endpoint);
        UdpClient.udpClient.SendEnvelope(envelope);
        history.add(envelope);
        state++;
    }
    
    public void resend() {
        try {
            UdpClient.udpClient.SendEnvelope(previousMessage);
        } catch (IOException ex) {
            Logger.getLogger(Conversation.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InterruptedException ex) {
            Logger.getLogger(Conversation.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public final int TotalRetries = 2;
    public int retries = 0;
    public int retryTimeout = 3000;
    public void processItems() {
        long time = System.currentTimeMillis();
        while(true) {
            //Get and process message
            Envelope envelope = null;
            envelope = IncomingQueue.poll();
            if(envelope != null) {
                processEnvelope(envelope);
                time = System.currentTimeMillis();
            }
            else {
                if(System.currentTimeMillis() - time > retryTimeout && retries < TotalRetries) {
                    resend();
                    retries++;
                    time = System.currentTimeMillis();
                }
            }
            
            //We done yet?
            boolean finished = isFinished();
            if(finished) {
                Player.ActivePlayer.Conversations.remove(this);
                break;
            }
            
            //Friggin exhausted
            try {
                Thread.sleep(50);
            } catch (InterruptedException ex) {
                Logger.getLogger(Conversation.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    
    public Class expectingType() {
        if(!isFinished()) {
            return MessageSequence.get(state);
        }
        return null;
    }
    
    public boolean isFinished() {
        int st = this.state;
        int size = MessageSequence.size();
        
        return st >= size || retries >= TotalRetries;
    }
}
