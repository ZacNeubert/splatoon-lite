/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Networking.Conversation;

import Extensions.Ext;
import Extensions.L;
import JavaMessage.Message;
import JavaMessage.MessageNumber;
import JavaMessage.ReplyMessages.BalloonReply;
import JavaMessage.ReplyMessages.GameListReply;
import JavaMessage.ReplyMessages.JoinGameReply;
import JavaMessage.RequestMessages.BuyBalloonRequest;
import JavaMessage.RequestMessages.FillBalloonRequest;
import JavaMessage.RequestMessages.GameListRequest;
import JavaMessage.RequestMessages.JoinGameRequest;
import JavaMessage.Routing;
import static Networking.Conversation.Conversation.player;
import Networking.Envelope;
import Networking.IPAddress;
import SharedObjects.Penny;
import dsoak.Player;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Zac
 */
public class FillBalloonConversation extends Conversation {
    public static String key = "FillBalloonConversation";
    
    public void initialize() {
        try {
            retryTimeout = 2000;
            state=0;
            sendEnvelope(MessageSequence.get(state));
        } catch (NoSuchMethodException ex) {
            L.l.log(Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            L.l.log(Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            L.l.log(Level.SEVERE, null, ex);
        } catch (IllegalArgumentException ex) {
            L.l.log(Level.SEVERE, null, ex);
        } catch (InvocationTargetException ex) {
            L.l.log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            L.l.log(Level.SEVERE, null, ex);
        } catch (InterruptedException ex) {
            L.l.log(Level.SEVERE, null, ex);
        }
    }
    
    public FillBalloonConversation() {
        history = new ArrayList<Envelope>();
        IncomingQueue = new ConcurrentLinkedQueue<>();
        if(MessageSequence == null) {
            MessageSequence = new ArrayList<Class>();
            MessageSequence.add(FillBalloonRequest.class);
            MessageSequence.add(BalloonReply.class);
        }
        try {
            player = Player.ActivePlayer;
            EndPoint = new IPAddress(player.ProxyJsonEndPoint);
            this.routingTo = player.getWaterSource();
        } catch (UnknownHostException ex) {
            Logger.getLogger(FillBalloonConversation.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    /*public Penny[] pennies;
    public void givePennies(Penny[] pennies) {
        this.pennies = pennies;
    }
    public void givePennies(Penny p1, Penny p2) {
        givePennies(new Penny[]{p1,p2});
    }*/
    
    @Override
    void processEnvelope(Envelope envelope) {
        Class MessageClass = MessageSequence.get(state);
        if(MessageClass == BalloonReply.class) {
            String message = envelope.getActualMessage();
            BalloonReply r = (BalloonReply) Ext.decodeJson(message, MessageClass);
            if(r.Success) {
                player.addAmmo(r.Balloon);
            }
            else {
                //player.addPenny(pennies[0]);
                //player.addPenny(pennies[1]);
            }
            state++;
        }
    }
}
