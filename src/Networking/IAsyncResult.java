package Networking;

public class IAsyncResult {
	
	public IAsyncResult(Object o) {
		AsyncState = o;
	}
	
	public Object AsyncState;
	public boolean CompletedSynchronously;
	public boolean IsCompleted;
}
