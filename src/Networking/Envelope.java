/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Networking;

import Extensions.Ext;
import JavaMessage.Message;
import JavaMessage.Routing;
import java.net.DatagramPacket;

/**
 *
 * @author Zac
 */
public class Envelope {
    public IPAddress Endpoint;
    public String Message;
    
    public String getActualMessage() {
        if(Message.contains("Routing:#Messages")) {
            //String actualMessage = Ext.extractElement(Message, "InnerMessage");
            String actualMessage = Ext.ghettoExtractElement(Message);
            return actualMessage;
        }
        else {
            return Message;
        }
    }
    
    public Envelope(DatagramPacket dp) {
        Endpoint = new IPAddress(dp.getAddress(), dp.getPort());
        Message = new String(dp.getData());
    }
    
    public Envelope(String message, IPAddress EndPoint) {
        this.Endpoint = EndPoint;
        this.Message = message;
    }
    
    public Envelope(Message message, IPAddress EndPoint) {
        Message = Ext.getJson(message);
        this.Endpoint = EndPoint;
    }
}
