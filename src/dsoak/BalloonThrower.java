/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dsoak;

import Extensions.Ext;
import Networking.Conversation.BuyBalloonConversation;
import Networking.Conversation.FillBalloonConversation;
import Networking.Conversation.ThrowBalloonConversation;
import SharedObjects.Penny;

/**
 *
 * @author Zac
 */
public class BalloonThrower extends Thread {
    
    Player player;
    
    public BalloonThrower(Player player) {
        this.player = player;
    }
    
    public void run() {
        while(player.getPlayerState() == Player.PlayerState.Playing) {
            ThrowBalloonConversation bbc = new ThrowBalloonConversation();
            player.addConversation(bbc, "Thrower");
            bbc.start();
            player.PurchaseAttempts++;
            while(bbc.isAlive()) {
                Ext.sleep(150);
                if(player.getPlayerState() != Player.PlayerState.Playing) {
                    break;
                }
            }
        }
    }
}
