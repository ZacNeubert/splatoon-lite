/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dsoak;

import Extensions.Ext;
import Networking.Conversation.BuyBalloonConversation;
import Networking.Conversation.FillBalloonConversation;
import SharedObjects.Penny;

/**
 *
 * @author Zac
 */
public class BalloonFiller extends Thread {
    
    Player player;
    
    public BalloonFiller(Player player) {
        this.player = player;
    }
    
    public void run() {
        while(player.getPlayerState() == Player.PlayerState.Playing) {
            FillBalloonConversation bbc = new FillBalloonConversation();
            player.addConversation(bbc, "filler");
            bbc.start();
            while(bbc.isAlive()) {
                Ext.sleep(150);
                if(player.getPlayerState() != Player.PlayerState.Playing) {
                    break;
                }
            }
        }
    }
}
