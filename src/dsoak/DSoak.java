/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dsoak;

import Extensions.Ext;
import Extensions.L;
import GUI.PlayerGUI;
import JavaMessage.Message;
import JavaMessage.RequestMessages.LoginRequest;
import Networking.IPAddress;
import SharedObjects.IdentityInfo;
import SharedObjects.ProcessInfo;
import SharedObjects.ProcessInfo.ProcessType;
import com.google.gson.*;
import java.io.File;
import java.io.FileNotFoundException;
import java.net.InetAddress;
import java.nio.charset.StandardCharsets;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author zacneubert
 */
public class DSoak extends Thread {
    /*
        Start and stop player(s)
        Set up logging stuff
    */
    
    /**
     * @param args the command line arguments
     */
    
    public PlayerGUI GUI;
    public String configFile = "config.txt";
    
    public void run() {
        File f = new File(configFile);
        String ANumber="";
        String First="";
        String Last="";
        String Alias="";
        String destIPString="";
        int destPort=0;
        try {
            Scanner sc = new Scanner(f);
            ANumber = sc.nextLine();
            First = sc.nextLine();
            Last = sc.nextLine();
            Alias = sc.nextLine();
            destIPString = sc.nextLine();
            destPort = Integer.parseInt(sc.nextLine());
        } catch (FileNotFoundException ex) {
            L.l.log(Level.SEVERE, null, ex);
        }
                
        IdentityInfo playerID = new IdentityInfo(
                ANumber,
                First,
                Last,
                Alias
        );
        String ProcessLabel = "Zac";
        ProcessType processType = ProcessType.Player;
        
        try {
            IPAddress localIP = new IPAddress(InetAddress.getByName("127.0.0.1"), 0);
            IPAddress destIP = new IPAddress(InetAddress.getByName(destIPString), destPort);
            
            L.l.info("LocalIP: " + localIP.Host.toString());
            L.l.info("DestIP: " + destIP.Host.toString());
            
            Player player = new Player(playerID, localIP, destIP, ProcessLabel);  
            PlayerGUI.player = player;
            player.start();
        }
        catch(Exception e) {
            L.l.log(Level.SEVERE, e.toString());
        }
    }
}
