/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dsoak;

import Extensions.Ext;
import Networking.Conversation.BuyBalloonConversation;
import SharedObjects.Penny;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Zac
 */
public class BalloonBuyer extends Thread {
    
    Player player;
    
    public BalloonBuyer(Player player) {
        this.player = player;
    }
    
    //int timeout=1500;
    public void run() {
        while(player.getPlayerState() == Player.PlayerState.Playing) {
            try {
                Thread.sleep(100);
            } catch (InterruptedException ex) {
                Logger.getLogger(BalloonBuyer.class.getName()).log(Level.SEVERE, null, ex);
            }
            BuyBalloonConversation bbc = new BuyBalloonConversation();
            player.addConversation(bbc, "Buyer");
            bbc.start();
            long time = System.currentTimeMillis();
            while(!bbc.isFinished()) {// && (System.currentTimeMillis() - time) < timeout) {
                Ext.sleep(150);
                if(player.getPlayerState() != Player.PlayerState.Playing) {
                    break;
                }
            }
        }
        int exited=0;
        exited++;
    }
}
